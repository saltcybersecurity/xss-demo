# SAST & DAST demo
## Goal
The goal of this demo is to show a build process that utilizes SAST (Static application security testing) and DAST (Dynamic application security testing). An example application is provided, that has a couple of issues, that can be picked up by the build process.

## The application
The demo application itself is very simple. It is made to look like an registration form, but is actually not linked to a user storage. The application accepts any username/password combo, and prints the username. 
This makes it vulnerable to a reflected xss attack, as the user can enter any html into the username field.

## Installation
The steps to install this demo are:  
1. Create a VM with ubuntu installed  
2. Install Jenkins  
3. Configure a Jenkins job for a DAST and SAST pipeline  
4. Install minikube  
5. Configure a deployment job in Jenkins  

If you are only interested in the DAST and SAST part, you can skip step 4 and 5.
The deployment steps are an example of deploying your application on a kubernetes cluster. The DAST and SAST parts, do not use kubernetes.
### Create an ubuntu VM
For the demo, we will need to install various tools. While it is possible to install these directly on your own machine, it is easier to use a VM. For the demo we will use an ubuntu focal fossa (20.04 LTS).

Download the desktop ISO image from https://ubuntu.com, and start a VM with it. You can use VMWare or virtualbox to run the VM. 
Give the VM at least 2 cores, and 4 GB RAM. The disk should be at least 20 GB.
Start the machine, and pick 'Install Ubuntu'. Pick 'Minimal installation'.
Continue untill you see the username promt. Pick the username 'salt'. Choose you own password.

Once restarted, open a terminal in the VM desktop environment (ctrl+alt+t shortcut), and run the following commands:  
`sudo apt update`  
`sudo apt install -y ca-certificates curl gnupg lsb-release conntrack openjdk-11-jdk-headless maven`  

Install docker:
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`  
    
`echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`  
  
`sudo apt update`  
`sudo apt install -y docker-ce docker-ce-cli containerd.io`  

Never ask for a password again, to become root:  
`echo "salt ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/salt-nopass`

### Install Jenkins
Run the following commands:  
`curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null`  
`echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null`  
`sudo apt update`  
`sudo apt install -y jenkins`  
`sudo usermod -a -G docker jenkins`  
`sudo service jenkins restart`  

These commands will install Jenkins, from the official repository. It will also allow jenkins to run docker.
Run the command `sudo cat /var/lib/jenkins/secrets/initialAdminPassword`
To get the initial password.
Open the browser within the VM (FireFox by default), and go to the URL http://localhost:8080
Follow the steps, and choose 'install suggested plugins'.

When creating a user, you can use any email. It is not verified.

### Configure Jenkins for the SAST and DAST demo

#### SAST
The SAST pipeline, does analysis on the code itself. It will not run the code. In the current set up the pipeline will build the project, and check if there are any vulnerabilities in all the dependencies. 

Install the OWASP dependency check using the following commands:  
`sudo mkdir -p /owasp`  
`sudo wget https://github.com/jeremylong/DependencyCheck/releases/download/v7.0.0/dependency-check-7.0.0-release.zip -O /owasp/dependency-check-7.0.0-release.zip`  
`cd /owasp && sudo unzip dependency-check-7.0.0-release.zip`  
`sudo chown -R jenkins:jenkins /owasp`  

Open the Jenkins dashboard (http://localhost:8080).
Click New Item.
Name the project demo-static-application-security-test. (Can be any name).
Click pipeline, and OK.
Under pipeline definition, choose from SCM.
Set the script path to JenkinsfileSAST.
Under SCM, select git.
Enter the URL https://bitbucket.org/saltcybersecurity/xss-demo.git

You should now be able to run the build.
After the build, you can find the report under Workspaces (after clicking on the build number). The file is called 'owasp-dependency-check-report.html'

##### Optional: Sonar
In the SAST build, you would typically also invoke sonar. Since this requires an external sonarcube server, this part is commented out. If you want to enable it, you would need to:  
- Install the  [SonarQube Scanner](https://plugins.jenkins.io/sonar) plugin in Jenkins.  
- Fork the demo repository.  
- Change the SCM url to your forked repository.  
- Change the pom.xml with your sonarcube project properties.  
- Create a sonarcube installation under Manage Jenkins > Global Tool configuration and Manage Jenkins > Configure System.  
- Change JenkinsfileSAST, and uncomment the sonar step.  


#### DAST
The dynamic security test will start the application, and run a series of scans and known attacks against the running code.

The DAST pipeline uses ansible to locally start a docker network containing both the application, and the scanner tool.
To install ansible, and the requirements run:  
`sudo apt install -y ansible python3-pip`  
`sudo pip install docker`  

Open the Jenkins dashboard (http://localhost:8080).
Click New Item.
Name the project demo-dynamic-application-security-test. The name does matter, as it is hardcoded in the ansible playbook.
Click pipeline, and OK.
Under pipeline definition, choose from SCM.
Set the script path to JenkinsfileDAST.
Under SCM, select git.
Enter the URL https://bitbucket.org/saltcybersecurity/xss-demo.git

You should now be able to run the build.

### Installing minikube
Minikube is used to demo a deployment on kubernetes. Usually the final step in the build process is deploying the application. Kubernetes is often used as platform.
This step is not required to run the SAST and DAST pipeline, but is here to make a complete demo.

To install minikube, run the following commands:  
`curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb`  
`sudo dpkg -i minikube_latest_amd64.deb`  

Allow jenkins to run minikube as root:  
`echo "jenkins ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/jenkins-nopass`  

Finally you should be able to start minikube:  
`sudo -u jenkins minikube start --driver=none`  
`sudo -u jenkins minikube addons enable registry`  
This will start minikube as the user 'jenkins'. This allows jenkins to deploy to the cluster, as it's OS lever user owns the cluster.

### Configuring the deployment job

Before running, or configuring the job, make sure that minikube is running using:
`sudo -u jenkins minikube status`
If not start with:
`sudo -u jenkins minikube start --driver=none`

Open the Jenkins dashboard (http://localhost:8080).
Click New Item.
Name the project demo-application. (Can be any name).
Click pipeline, and OK.
Under pipeline definition, choose from SCM.
Set the script path to JenkinsfilePrd.
Under SCM, select git.
Enter the URL https://bitbucket.org/saltcybersecurity/xss-demo.git

You should now be able to run the job.
When successful, it should have deployed the app to the local minikube cluster. To check this, run the command:
`sudo -u jenkins minikube service --all`
Copy the URL, and open that in the browser. Add /xss-demo/ (Full example URL: http://10.0.2.15:32420/xss-demo/), to see the application.

## The demo
The demo repository contains two branches: master and fixed. The master branch has the version of the application that is vulnerable to XSS, and has vulnerable dependencies. The fixed branch contains the same application, but with fixes for these flaws. Using the two branches we can see the difference.

1. Deploy the application using the master branch, using the JenkinsPrd pipeline. Enter some HTML (for example `<script>alert('hacked');</script>`) This will show that the application is vulnerable.
2. Run the SAST and DAST pipeline. These should show the XSS vulnerability, and the vulnerable dependencies.
3. Change the branch of all the pipelines to 'fixed'. For example for the SAST job: Go to the Job > Configure. Scroll down to Branch Specifier, and change it to '*/fixed'.
4. Re run SAST and DAST and see the difference.
5. Re deploy the application using JenkinsPrd, and check if the XSS vulnerability is still there.

## Disclaimer
Don't use any of this code in a live environment. This code is for demonstration purposes only.





