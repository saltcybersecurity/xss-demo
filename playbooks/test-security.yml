---
- name: Run xss-demo application in docker for pentests delegated by jenkins
  hosts: localhost
  connection: local
  vars: 
     base_image: openjdk:8-jdk-alpine
     website_test_url: http://172.18.0.10:9080/xss-demo
     website_image_name: jenkins/xssdemo
     website_container_name: jenkins-xssdemo-test     
     owasp_zap_image_name: owasp/zap2docker-stable
     scan_name: myzapscan
     scan_type:
       baseline_scan: zap-baseline.py
       full_scan: zap-full-scan.py
     reports_location: /tmp

     
     
  tasks:
    # Create a network so that the docker containers can communicate with each other.
    # We need this in order for the zap proxy to scan the xssdemo docker container
    - name: Create a network
      docker_network:
        name: testnetwork
        ipam_config:
          - subnet: 172.18.0.0/16
            gateway: 172.18.0.1

      # we need to keep the openjdk:8-jdk-alpine container image up to date,
      # because the 'jenkins/xssdemo' image is based on the openjdk:8-jdk-alpine container.
    - name: Pull latest openjdk:8-jdk-alpine image
      docker_image:
        name: "{{ base_image }}"
        source: pull
        tag: latest
        state: present
        # As 'latest' usually already is present, we need to enable overwriting of existing tags:
        force_tag: yes        
        
    - name: Build xss-demo test image
      docker_image:
        build:
          # path is './' when playbook is started by jenkins
          # or '/var/lib/jenkins/workspace/demo-dynamic-application-security-test' when started manually
          path: /var/lib/jenkins/workspace/demo-dynamic-application-security-test
          pull: no
        name: "{{ website_image_name }}"
        source: build
        state: present
        force_source: yes
        
    - name: Run test application on "{{ website_test_url }}"
      docker_container:
        name: "{{ website_container_name }}"
        networks:
        - name: testnetwork 
          ipv4_address: "172.18.0.10"
        networks_cli_compatible: yes
        state: started
        image: "{{ website_image_name }}"
        restart: yes

 
    - name: Run owasp zap scan against "{{ website_test_url }}"
      docker_container:
        name: "{{ scan_name }}"
        networks:
        - name: testnetwork
          ipv4_address: "172.18.0.11"
        networks_cli_compatible: yes  
        image: "{{ owasp_zap_image_name }}"
        interactive: yes
        auto_remove: yes
        state: started
        volumes: "{{ reports_location }}:/zap/wrk/:rw"
        command: "{{ scan_type.full_scan }} -t {{ website_test_url }} -r {{ scan_name }}_report.html"
    
    - name: Getting raw output of the scan 
      command: "docker logs -f {{ scan_name }}"
      register: scan_output 

    - debug:
        msg: "{{ scan_output }}"

    - name: Stop and remove test application container
      docker_container:
        name: "{{ website_container_name }}" 
        state: absent
