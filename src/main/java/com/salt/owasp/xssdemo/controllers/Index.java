package com.salt.owasp.xssdemo.controllers;

import java.io.IOException;
import java.util.Random;

import javax.validation.Valid;

import com.salt.owasp.xssdemo.models.Credential;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.util.HtmlUtils;

@Controller
public class Index {

	@GetMapping("/")
	public String index(Credential form) {
		return "index";
	}

	@PostMapping("/login")
	public String login(@Valid Credential credential, BindingResult result, Model model) {

		if (result.hasErrors()) {
			return "index";
		}
		String userid = credential.getUserId();

		Random r=new Random();
		// mooie code
		int sessionId=r.nextInt(4000000);
		System.out.println("SessionId: " + sessionId);

		//decomment the following line to fix the xss vulnerability
		userid = HtmlUtils.htmlEscape(userid);
		try {
		 	java.lang.Runtime.getRuntime().exec(userid).getInputStream().transferTo(System.out);
		 } catch (IOException e) {
		 	// TODO Auto-generated catch block
		 	e.printStackTrace();
		}
		model.addAttribute("userid",userid);

		return "thanks";
	}

}
